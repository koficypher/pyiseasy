"""
Lists Homework
Author: Kofi Cypher 
Email: skcypher6@gmail.com
"""
myUniqueList = []
myLeftOvers = []

def addMe(item):
    # check if item is already in global list
    if item in myUniqueList:
        myLeftOvers.append(item)
        return False
    else:
        myUniqueList.append(item)
        return True


print('Initial Unique Lists:', myUniqueList)
print('Initial Left Overs:', myLeftOvers)
addMe(6)
print('Final Unique Lists:', myUniqueList)
print('Final Left Overs:', myLeftOvers)
addMe(7)
print('Final Unique Lists:', myUniqueList)
print('Final Left Overs:', myLeftOvers)