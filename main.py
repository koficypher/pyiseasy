"""
Variables Assignment
Author: Kofi Cypher
Email: skcypher6@gmail.com
"""
# song title
title = 'Little Havanna'

#nummber
number = 3

#artist's name
artist = 'Jon Soul'

#duration or length
duration = '3 mins 28 secs'

#genre
genre = 'RnB'

#Album
album = 'HillSide Vibes'

#record label
label = 'StoneView Records'

#published date
publishedDate = '2016-06-04'

#year released
year = '2016'

#publisher
publisher = 'StoneView Marketing'



print(title)
print(number)
print(artist)
print(duration)
print(genre)
print(album)
print(label)
print(publishedDate)
print(year)
print(publisher)