"""
Functions Assignment
Author: Kofi Cypher
Email: skcypher6@gmail.com
"""

#function that returns the artist name
def Artist():
    print('Jon Soul')

#function that returns the song duration
def Duration():
    print('3 mins 28 secs')

#function that returns the album name
def Album():
    print('HillSide Vibes')

#function that shows whether song has been published or not
def isPublished():
    print(True)

#test 
Album()
isPublished()
